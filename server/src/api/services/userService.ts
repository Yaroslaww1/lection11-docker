import lowdb from 'lowdb';
import { IDatabase, db } from '../../data/connection';
import { IUser } from '../../data/models';

class UserService {
  constructor(
    private db: lowdb.LowdbSync<IDatabase>
  ) {}

  async getAllUsers(): Promise<IUser[]> {
    const users = this.db.get('users')
                          .value();
    return users;
  }

  private async find(param: string, value: string): Promise<IUser> {
    const user = this.db.get('users')
                        .find({ [param]: value })
                        .value();
    return user;
  }

  async getUserById(id: string): Promise<IUser> {
    const user = await this.find('id', id);
    return user;
  }

  async getUserByUsername(username: string): Promise<IUser> {
    const user = await this.find('username', username);
    return user;
  }

  async deleteUserById(id: string): Promise<IUser> {
    const user = this.db.get('users')
                        .find({ id })
                        .value();
    db.get('users')
      .remove({ id })
      .write();
    return user;
  }

  async addUser(user: IUser): Promise<IUser> {
    db.get('users')
      .push(user)
      .write();
    return user
  }

  async updateUserById(id: string, newUser: IUser): Promise<IUser> {
    this.db.get('users')
            .find({ id })
            .assign(newUser)
            .write();
    const user = await this.getUserById(id);
    return user;
  }
}

const userService = new UserService(db);
export default userService;