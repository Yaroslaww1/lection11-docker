import lowdb from 'lowdb';
import { IDatabase, db } from '../../data/connection';
import { ILike } from '../../data/models';

class LikeService {
  constructor(
    private db: lowdb.LowdbSync<IDatabase>
  ) {}

  async getIsLiked(obj: { messageId: string, userId: string }): Promise<boolean> {
    const isLiked = this.db.get('likes')
                            .find(obj)
                            .value()
    if (isLiked)
      return true;
    else
      return false;
  }

  async addOrUpdateLike(obj: { messageId: string, userId: string }): Promise<void> {
    console.log('addOrUpdate', obj);
    const isLiked = await this.getIsLiked(obj);
    console.log(isLiked)
    if (isLiked) 
      this.db.get('likes')
              .remove(obj)
              .write();
    else
      this.db.get('likes')
              .push(obj)
              .write()
  }
}

const likeService = new LikeService(db);
export default likeService;