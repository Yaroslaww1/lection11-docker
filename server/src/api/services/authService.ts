import lowdb from 'lowdb';
import { IDatabase, db } from '../../data/connection';
import { IAuthToken, IUser } from '../../data/models';
import { v4 as uuidv4 } from 'uuid';

class AuthService {
  constructor(
    private db: lowdb.LowdbSync<IDatabase>
  ) {}

  async addAuthToken(user: IUser): Promise<IAuthToken> {
    const token = uuidv4();
    const authToken: IAuthToken = {
      token,
      user
    };
    const existingAuthToken = this.db.get('authTokens')
                                      .find({ user })
                                      .value();
    if (!existingAuthToken)
      this.db.get('authTokens')
              .push(authToken)
              .write();
    else 
      this.db.get('authTokens')
              .find({ user })
              .assign({ token })
              .write();
    return authToken;
  }

  async getUserByToken(token: string): Promise<IUser | null> {
    const authToken = this.db.get('authTokens')
                             .find({ token })
                             .value();
    const user = authToken.user;
    return user;
  }
}

const authService = new AuthService(db);
export default authService;