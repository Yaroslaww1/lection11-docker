import { Request, Response, NextFunction } from 'express';
import asyncHandler from '../helpers/asyncHandler';
import authService from '../services/authService';

export default asyncHandler(async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader || !authHeader.startsWith("Bearer ")) 
      throw new Error('Login failed, authorization header is not set');
    const token = authHeader.substring(7, authHeader.length);
    const user = await authService.getUserByToken(token);
    if (!user)
      throw new Error('Login failed, wrong token');
    req.user = user;
    next();
  } catch (error) {
    next(error);
  }
});
