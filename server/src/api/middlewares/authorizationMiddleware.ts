import { Request, Response, NextFunction } from 'express';
import authenticationMiddleware from './authenticationMiddleware';

const isPathEqual = (route: string, reqPath: string) => {
  const routeArr = route.split('/');
  const reqPathArr = reqPath.split('/');
  if (routeArr.length > reqPathArr.length) {
    return false;
  }

  for (let i = 0; i < reqPathArr.length; i++) {
    if (reqPathArr[i] === routeArr[i]) {
      continue;
    }

    if (routeArr[i] === '*') {
      return true;
    }
    return false;
  }
  return true;
};

export default (routesWhiteList: string[] = []) => (req: Request, res: Response, next: NextFunction): void => (
  routesWhiteList.some(route => isPathEqual(route, req.path))
    ? next()
    : authenticationMiddleware(req, res, next)
);
