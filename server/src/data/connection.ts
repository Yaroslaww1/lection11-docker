import lowdb from 'lowdb';
import FileSync from "lowdb/adapters/FileSync";
import { IUser, IMessage, IAuthToken, ILike } from './models';

export interface IDatabase {
  users: IUser[]
  messages: IMessage[]
  authTokens: IAuthToken[]
  likes: ILike[]
}

const adapter = new FileSync<IDatabase>('db.json');
export const db = lowdb(adapter);

import { data } from './seededData';

db.defaults(data).write();
