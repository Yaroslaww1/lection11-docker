export interface IUser {
  id: string
  username: string
  password: string
  avatar: string
  isAdmin: boolean
}

export interface IMessage {
  id: string
  user: IUser
  text: string
  createdAt: string
  editedAt: string
}

export interface IAuthToken {
  token: string
  user: IUser
}

export interface ILike {
  messageId: string
  userId: string
}