export const APP_PORT = 3330;

export const ROUTES_WHITE_LIST = [
  '/auth/login'
];

export const CORS_OPTIONS = {
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}