export function isRequest(reducerPrefix: string, action: string): boolean {
  const prefix = action.split('/')[0];
  action = action.substring(prefix.length + 1);
  if (prefix !== reducerPrefix)
    return false;
  const parts = action.split('_');
  const type = parts[parts.length - 1];
  if (type === 'REQUEST')
      return true;
  else
      return false;
}

export function isError(reducerPrefix: string, action: string): boolean {
  const prefix = action.split('/')[0];
  action = action.substring(prefix.length + 1);
  if (prefix !== reducerPrefix)
    return false;
  const parts = action.split('_');
  const type = parts[parts.length - 1];
  if (type === 'ERROR')
      return true;
  else
      return false;
}