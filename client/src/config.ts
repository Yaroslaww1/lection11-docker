export const SERVER_URL = 'http://localhost:3330';
export const LOGIN_URL = `${SERVER_URL}/api/auth/login`;
export const FETCH_USER_URL = `${SERVER_URL}/api/auth/user`;

export const USERS_URL = `${SERVER_URL}/api/users`;
export const MESSAGES_URL = `${SERVER_URL}/api/messages`;