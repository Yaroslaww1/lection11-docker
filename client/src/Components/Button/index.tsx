import React from 'react';

import "./styles.css";

interface Props {
  type: string
  onClick: () => void
}

const Button: React.FC<Props> = ({
  type,
  onClick,
  children
}) => {
  return (
    <button
      className={type}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button;
