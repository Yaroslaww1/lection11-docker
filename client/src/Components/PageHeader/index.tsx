import React from 'react';

import "./styles.css";

const PageHeader: React.FC = () => {
  const onLogout = () => {
    localStorage.removeItem('token');
    window.location.reload();
  }

  return (
    <div className="page-header">
      <img src="./img/bsa-logo.png" alt="logo"></img>
      <button
        onClick={onLogout}
      >
        Logout
      </button>
    </div>
  );
}

export default PageHeader;
