import React from 'react';
import PropTypes from 'prop-types';

import "./styles.css";

type LikeIconProps = {
  isLiked: boolean,
  onLike: () => void
}

const LikeIcon: React.FC<LikeIconProps> = ({
  isLiked,
  onLike
}) => {
  const style = {
    color: isLiked ? 'red' : ''
  }

  return (
    <div className="message-icon like">
      <i
        className="fa fa-heart"
        aria-hidden="true"
        style={style}
        onClick={onLike}
      ></i>
    </div>
  );
}

LikeIcon.propTypes = {
  isLiked: PropTypes.bool.isRequired,
  onLike: PropTypes.func.isRequired
}

export default LikeIcon;
