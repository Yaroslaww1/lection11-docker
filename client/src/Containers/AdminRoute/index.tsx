/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import Spinner from '../../Components/Spinner';
import { RootState } from '../../store';

const mapState = (state: RootState) => ({
  isAdmin: state.system.currentUser?.isAdmin,
  isLoading: state.system.isLoading,
  isAuth: state.system.isAuthorized
});

const mapDispatch = {}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {
  component: any,
  [x:string]: any;
};

const AdminRoute: React.FC<Props> = ({ 
  component: Component,
  isAdmin,
  isLoading,
  isAuth,
  ...rest 
}) => {
  if (isLoading)
    return <Spinner />

  const getRedirect = (props: any) => {
    if (isAdmin && isAuth) 
      return <Component {...props} />
    if (isAuth && !isAdmin)
      return <Redirect to={{ pathname: '/' }} />
    return <Redirect to={{ pathname: '/login' }} />
  }

  return (
    <Route
      {...rest}
      render={props => getRedirect(props)}
    />
  )
};

export default connector(AdminRoute);
