/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../store';

const mapState = (state: RootState) => ({
  isAuthorized: state.system.isAuthorized,
  isAdmin: state.system.currentUser?.isAdmin
});

const mapDispatch = {}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {
  component: any,
  [x:string]: any;
};

const PublicRoute: React.FC<Props> = ({ 
  component: Component,
  isAuthorized,
  isAdmin,
  ...rest
}) => {
  const getRedirect = (props: any) => {
    if (!isAdmin && !isAuthorized) 
      return <Component {...props} />
    if (isAuthorized && !isAdmin)
      return <Redirect to={{ pathname: '/' }} />
    return <Redirect to={{ pathname: '/users' }} />
  }

  return (
    <Route
      {...rest}
      render={props => getRedirect(props)}
    />
  )
}

export default connector(PublicRoute);
