import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { chatReducer } from './Scenes/Chat/reducer';
import { systemReducer } from './System/reducer';
import { loginPageReducer } from './Scenes/Login/reducer';
import { usersListPageReducer } from './Scenes/UsersListPage/reducer';
import { userPageReducer } from './Scenes/UserPage/reducer';
import { messagePageReducer } from './Scenes/MessagePage/reducer';

import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';
import loginPageSagas from './Scenes/Login/sagas';
import systemSagas from './System/sagas';
import userListPageSagas from './Scenes/UsersListPage/sagas';
import userPageSagas from './Scenes/UserPage/sagas';
import chatPageSagas from './Scenes/Chat/sagas';
import messagePageSagas from './Scenes/MessagePage/sagas';

const reducers = {
  chat: chatReducer,
  system: systemReducer,
  loginPage: loginPageReducer,
  usersListPage: usersListPageReducer,
  userPage: userPageReducer,
  messagePage: messagePageReducer
};

const rootReducer = combineReducers({
  ...reducers
});

function* rootSaga() {
  yield all([
    loginPageSagas(),
    systemSagas(),
    userListPageSagas(),
    userPageSagas(),
    chatPageSagas(),
    messagePageSagas()
  ])
};

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

export type RootState = ReturnType<typeof rootReducer>;

export default store;
