import { IUser } from "../interfaces";
import { SystemActions, SystemActionTypes } from "./types";

export function fetchCurrentUser(): SystemActions {
  return {
    type: SystemActionTypes.FETCH_CURRENT_USER_REQUEST
  }
}

export function setCurrentUser(user: IUser): SystemActions {
  return {
    type: SystemActionTypes.FETCH_CURRENT_USER_SUCCESS,
    user
  }
}

export function setError(error: string): SystemActions {
  return {
    type: SystemActionTypes.FETCH_CURRENT_USER_ERROR,
    error
  }
}