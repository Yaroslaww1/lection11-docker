import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps  } from 'react-router-dom';
import Button from '../../Components/Button';
import ErrorElement from '../../Components/Error';
import Spinner from '../../Components/Spinner';
import { RootState } from '../../store';
import { fetchUsers, deleteUser } from './actions';

import './styles.css';
import UserItem from './userItem';

const mapState = (state: RootState) => ({
  isLoading: state.usersListPage.isLoading,
  users: state.usersListPage.users,
  error: state.usersListPage.error
});

const mapDispatch = {
  fetchUsers,
  deleteUser
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & RouteComponentProps;

const UsersListPage: React.FC<Props> = ({
  isLoading,
  users,
  fetchUsers: fetchUsersProps,
  history,
  deleteUser: deleteUserProps,
  error
}) => {
  useEffect(() => {
    if (!users || users.length === 0) {
      fetchUsersProps()
    }
  })

  if (isLoading)
    return (
      <Spinner />
    )

  const onEdit = (id: string) => {
    history.push(`/user/${id}/`);
  }

  const onDelete = (id: string) => {
    deleteUserProps(id);
  }

  const onAdd = () => {
    history.push('/user/');
  }

  return (
    <div className="users-list">
      <Button
        type='success add-user-button'
        onClick={onAdd}
      >
        Add user
      </Button>
      {
        users.map(user => (
            <UserItem 
              key={user.id}
              user={user}
              onEdit={() => onEdit(user.id)}
              onDelete={() => onDelete(user.id)}
            />
        ))
      }
      <ErrorElement
        error={error}
      />
    </div>
  )
}

export default connector(UsersListPage);
