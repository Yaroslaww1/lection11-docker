import { IUser } from "../../interfaces";
import { UserListActions, UserListActionTypes } from "./types";

export function fetchUsers(): UserListActions {
  return {
    type: UserListActionTypes.FETCH_USERS_REQUEST
  }
}

export function deleteUser(userId: string): UserListActions {
  return {
    type: UserListActionTypes.DELETE_USER_REQUEST,
    userId
  }
}

export function setUsers(users: IUser[]): UserListActions {
  return {
    type: UserListActionTypes.FETCH_USERS_SUCCESS,
    users
  }
}

export function setError(error: string): UserListActions {
  return {
    type: UserListActionTypes.ERROR,
    error
  }
}