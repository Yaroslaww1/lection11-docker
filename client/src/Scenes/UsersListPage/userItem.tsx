import React from 'react';
import Button from '../../Components/Button';
import { IUser } from '../../interfaces';

import './styles.css';

export interface Props {
  user: IUser
  onEdit: () => void
  onDelete: () => void
}

const UserItem: React.FC<Props> = ({
  user,
  onEdit,
  onDelete
}) => {
  return (
    <div className="user">
      <div className="user-username">
        {user.username}
      </div>
      <Button
        type='info'
        onClick={onEdit}
      >
        Edit
      </Button>
      <Button
        type='danger'
        onClick={onDelete}
      >
        Delete
      </Button>
    </div>
  )
}

export default UserItem;
