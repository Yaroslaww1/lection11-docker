import { IUser } from '../../interfaces';

export enum UserListActionTypes {
  ERROR = 'UserListActionTypes/ERROR',
  FETCH_USERS_REQUEST = 'UserListActionTypes/FETCH_USERS_REQUEST',
  FETCH_USERS_SUCCESS = 'UserListActionTypes/FETCH_USERS_SUCCESS',
  DELETE_USER_REQUEST = 'UserListActionTypes/DELETE_USER_REQUEST',
  DELETE_USER_SUCCESS = 'UserListActionTypes/DELETE_USER_SUCCESS',
}

export interface IFetchUsersRequestAction {
  type: UserListActionTypes.FETCH_USERS_REQUEST
}

export interface IDeleteUserRequestAction {
  type: UserListActionTypes.DELETE_USER_REQUEST
  userId: string
}

interface IFetchUsersAction {
  type: UserListActionTypes.FETCH_USERS_SUCCESS
  users: IUser[]
}

interface IDeleteUserAction {
  type: UserListActionTypes.DELETE_USER_SUCCESS
  user: IUser
}

interface IErrorAction {
  type: UserListActionTypes.ERROR
  error: string
}

type UserListRequests = 
  IFetchUsersRequestAction
| IDeleteUserRequestAction;

type UserListSuccess = 
  IFetchUsersAction
| IDeleteUserAction;

export type UserListErrors = IErrorAction;

export type UserListActions = 
  UserListRequests
| UserListSuccess
| UserListErrors

export interface UserListState {
  users: IUser[]
  error: string,
  isLoading: boolean
}