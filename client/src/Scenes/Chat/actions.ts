import { IMessage } from "../../interfaces";
import { ChatActions, ChatActionTypes } from "./types";

export function fetchMessages(userId: string): ChatActions {
  return {
    type: ChatActionTypes.FETCH_MESSAGES_REQUEST,
    userId
  }
}

export function addMessage(text: string, userId: string): ChatActions {
  return {
    type: ChatActionTypes.ADD_MESSAGE_REQUEST,
    text,
    userId
  }
}

export function toggleLike(messageId: string, userId: string): ChatActions {
  return {
    type: ChatActionTypes.LIKE_MESSAGE_REQUEST,
    messageId,
    userId
  }
}

export function setMessages(messages: IMessage[]): ChatActions {
  return {
    type: ChatActionTypes.FETCH_MESSAGES_SUCCESS,
    messages
  }
}

export function setError(error: string): ChatActions {
  return {
    type: ChatActionTypes.ERROR,
    error
  }
}