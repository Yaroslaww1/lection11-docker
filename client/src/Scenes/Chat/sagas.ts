import Api from '../../helpers/apiHelper';
import { MESSAGES_URL } from '../../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { ChatActionTypes, IAddMessageRequestAction, IFetchMessagesRequestAction, ILikeMessageRequestAction } from './types';
import { fetchMessages, setError, setMessages } from './actions';

function* fetchMessagesHandler(action: IFetchMessagesRequestAction) {
	try {
		const { userId } = action;
		const { response, error: errorMessage } = yield call(Api.get, `${MESSAGES_URL}/${userId}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { messages } = response.data;
		yield put(setMessages(messages));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchFetchMessages() {
	yield takeEvery(ChatActionTypes.FETCH_MESSAGES_REQUEST, fetchMessagesHandler)
}

function* likeMessagesHandler(action: ILikeMessageRequestAction) {
	try {
		const { type, ...data } = action;
		const { error: errorMessage } = yield call(Api.post, `${MESSAGES_URL}/likes`, data);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		yield put(fetchMessages(data.userId));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchLikeMessage() {
	yield takeEvery(ChatActionTypes.LIKE_MESSAGE_REQUEST, likeMessagesHandler)
}

function* addMessagesHandler(action: IAddMessageRequestAction) {
	try {
		const { type, ...data } = action;
		const { response, error: errorMessage } = yield call(Api.post, `${MESSAGES_URL}`, data);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { message } = response.data;	
		yield put(fetchMessages(message.user.id));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchAddMessage() {
	yield takeEvery(ChatActionTypes.ADD_MESSAGE_REQUEST, addMessagesHandler)
}

export default function* chatPageSagas() {
	yield all([
		watchFetchMessages(),
		watchLikeMessage(),
		watchAddMessage()
	])
};