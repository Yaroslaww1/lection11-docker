import React, { useCallback, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps  } from 'react-router-dom';
import Spinner from '../../Components/Spinner';
import ChatHeader from '../../Components/ChatHeader';
import ChatMain from '../../Components/ChatMain';
import ChatInput from '../../Containers/ChatInput';

import "./styles.css";
import { 
  fetchMessages,
  toggleLike
} from './actions';
import { RootState } from '../../store';
import { IMessage } from '../../interfaces';
import { addMessage } from './actions';

const mapState = (state: RootState) => ({
  messages: state.chat.messages,
  currentUser: state.system.currentUser,
  isLoading: state.chat.isLoading,
  error: state.chat.error
});

const mapDispatch = {
  fetchMessages,
  toggleLike,
  addMessage
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & RouteComponentProps;

const Chat: React.FC<Props> = ({
  messages,
  currentUser,
  isLoading,
  error,
  history,
  fetchMessages: fetchMessagesProps,
  toggleLike: toggleLikeProps,
  addMessage: addMessageProps
}) => {
  const openEditingPage = (message: IMessage) => {
    history.push(`/message/${message.id}`)
  }

  const keyboardListener = useCallback((event: KeyboardEvent) => {
    const keyCode = event.code;
    if (keyCode === 'ArrowUp') {
      const lastMessageIndex = messages.reduce((acc, message, index) => (
        message.user.id === currentUser!.id ? index : acc
      ), -1);
      if (lastMessageIndex > 0) {
        const lastMessage = messages[lastMessageIndex];
        openEditingPage(lastMessage);
      }
    }
  }, [messages, currentUser])

  useEffect(() => {
    if (!error && (!messages || messages.length === 0)) {
      fetchMessagesProps(currentUser!.id);
    }
  });

  useEffect(() => {
    document.addEventListener('keydown', keyboardListener);
    return () => document.removeEventListener('keydown', keyboardListener);
  }, [keyboardListener])

  if (isLoading)
    return (
      <Spinner />
    )

  const toggleLike = (messageId: string) => toggleLikeProps(messageId, currentUser!.id);

  return (
    <div className="chat-wrapper">
      <ChatHeader
        messages={messages}
        chatName="Chat name"
      />
      <ChatMain
        messages={messages}
        currentUser={currentUser!}
        messageFunction={{
          onLike: toggleLike,
          onEdit: openEditingPage
        }}
      />
      <ChatInput 
        addMessage={(text: string) => addMessageProps(text, currentUser!.id)}
      />
    </div>
  );
}

export default connector(Chat);
