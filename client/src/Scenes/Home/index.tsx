import React from 'react';
import Chat from '../Chat';
import PageHeader from '../../Components/PageHeader';
import PageFooter from '../../Components/PageFooter';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const HomePage: React.FC = (props: any) => {
  return (
    <>
      <PageHeader />
      <Chat {...props}/>
      <PageFooter />
    </>
  );
}

export default HomePage;
