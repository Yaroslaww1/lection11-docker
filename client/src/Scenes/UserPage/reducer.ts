import { UserErrors, UserActions, UserActionTypes, UserState } from './types';
import { isRequest, isError } from '../../helpers/reduxHelper';

const initialState: UserState = {
  user: {
    id: '',
    username: '',
    avatar: '',
    isAdmin: false,
    password: ''
  },
  error: '',
  isLoading: false
}

export function userPageReducer(
  state = initialState,
  action: UserActions
): UserState {
  if (isRequest('UserActionTypes', action.type)) 
    return {
      ...state,
      isLoading: true
    }
  if (isError('UserActionTypes', action.type)) { 
    action = action as UserErrors;
    return {
      ...state,
      error: action.error,
      isLoading: false
    }
  }
  switch (action.type) {
    case UserActionTypes.FETCH_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.user
      }
    default:
      return state;
  }
}