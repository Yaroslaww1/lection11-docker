import Api from '../../helpers/apiHelper';
import { MESSAGES_URL } from '../../config';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { MessageActionTypes, IUpdateMessageRequestAction, IFetchMessageRequestAction, IDeleteMessageRequestAction } from './types';
import { setError, setMessage } from './actions';
import { fetchMessages } from '../Chat/actions';
import { getDefaultMessage } from '../../Services/messageService';

function* fetchMessageHandler(action: IFetchMessageRequestAction) {
	try {
		const { response, error: errorMessage } = yield call(Api.get, `${MESSAGES_URL}/message/${action.messageId}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { message } = response.data;
		yield put(setMessage(message));
		yield put(fetchMessages(action.userId));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchFetchMessage() {
	yield takeEvery(MessageActionTypes.FETCH_MESSAGE_REQUEST, fetchMessageHandler)
}

function* updateMessageHandler(action: IUpdateMessageRequestAction) {
	try {
		const { response, error: errorMessage } = yield call(Api.put, `${MESSAGES_URL}/message/${action.message.id}`, action.message);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		const { message } = response.data;
		yield put(setMessage(message));
		yield put(fetchMessages(action.userId));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchUpdateMessage() {
	yield takeEvery(MessageActionTypes.UPDATE_MESSAGE_REQUEST, updateMessageHandler)
}

function* deleteMessageHandler(action: IDeleteMessageRequestAction) {
	try {
		const { error: errorMessage } = yield call(Api.delete, `${MESSAGES_URL}/message/${action.messageId}`);
		
		if (errorMessage) 
			throw new Error(errorMessage);	

		yield put(setMessage(getDefaultMessage()));
		yield put(fetchMessages(action.userId));
	} catch (error) {
		yield put(setError(error.message));
	}
}

function* watchDeleteMessage() {
	yield takeEvery(MessageActionTypes.DELETE_MESSAGE_REQUEST, deleteMessageHandler)
}

export default function* messagePageSagas() {
	yield all([
		watchFetchMessage(),
		watchUpdateMessage(),
		watchDeleteMessage()
	])
};