import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps  } from 'react-router-dom';
import Button from '../../Components/Button';
import ErrorElement from '../../Components/Error';
import Spinner from '../../Components/Spinner';
import { IMessage } from '../../interfaces';
import { RootState } from '../../store';
import { fetchMessage, updateMessage, setMessage, deleteMessage } from './actions';

import './styles.css';

const mapState = (state: RootState) => ({
  isLoading: state.messagePage.isLoading,
  message: state.messagePage.message,
  error: state.messagePage.error,
  currentUser: state.system.currentUser
});

const mapDispatch = {
  fetchMessage,
  updateMessage,
  deleteMessage,
  setMessage
}

interface IMatchParams {
  id: string
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & RouteComponentProps<IMatchParams>;

const MessagePage: React.FC<Props> = ({
  fetchMessage: loadMessage,
  isLoading,
  match,
  history,
  currentUser,
  message: messageProps,
  updateMessage: updateMessageProps,
  setMessage: setMessageProps,
  deleteMessage: deleteMessageProps,
  error
}) => {
  const [message, setMessage] = useState<IMessage>(messageProps);
  useEffect(() => {
    if (match.params.id) {
      loadMessage(match.params.id, currentUser!.id);
    }
  }, [match.params.id, loadMessage, currentUser]);

  useEffect(() => {
    setMessage(messageProps);
  }, [messageProps])

  if (isLoading)
    return (
      <Spinner />
    )

  const onChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = event.currentTarget;
    setMessage({
      ...message,
      [name]: value
    })
  }

  const onBack = () => {
    history.push('/');
  }

  const onSave = () => {
    updateMessageProps(message, currentUser!.id);
  }

  const onDelete = () => {
    deleteMessageProps(message.id, currentUser!.id);
    history.push('/');
  }

  return (
    <div className="message-page-wrapper">
      <div className="content">
        <textarea
          id="text"
          value={message.text}
          onChange={onChange}
          name="text"
        />
        <div className="buttons">
          <Button
            type='info'
            onClick={onBack}
          >
            Back
          </Button>
          <Button
            type='danger'
            onClick={onDelete}
          >
            Delete
          </Button>
          <Button
            type='success'
            onClick={onSave}
          >
            Save
          </Button>
        </div>
        <ErrorElement
          error={error}
        />
      </div>
    </div>
  )
}

export default connector(MessagePage);
