import { IUser } from '../../interfaces';

export enum LoginActionTypes {
  LOGIN_USER = 'LOGIN_USER',
  LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS',
  LOGIN_USER_ERROR = 'LOGIN_USER_ERROR'
}

export interface IFetchCurrentUserAction {
  type: LoginActionTypes.LOGIN_USER
  username: string
  password: string
}

interface IFetchCurrentUserSuccessAction {
  type: LoginActionTypes.LOGIN_USER_SUCCESS
  user: IUser
}

interface IFetchCurrentUserErrorAction {
  type: LoginActionTypes.LOGIN_USER_ERROR
  error: string
}

export type LoginActions = 
  IFetchCurrentUserAction
| IFetchCurrentUserSuccessAction
| IFetchCurrentUserErrorAction;

export interface LoginState {
  error: string
}