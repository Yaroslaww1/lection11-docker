import { LoginActions, LoginActionTypes, LoginState } from './types';

const initialState: LoginState = {
  error: ''
}

export function loginPageReducer(
  state = initialState,
  action: LoginActions
): LoginState {
  switch (action.type) {
    case LoginActionTypes.LOGIN_USER_ERROR: 
      return {
        ...state,
        error: action.error
      }
    default:
      return state;
  }
}