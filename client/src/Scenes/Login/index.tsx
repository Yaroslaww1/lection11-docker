import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Button from '../../Components/Button';
import ErrorElement from '../../Components/Error';
import Spinner from '../../Components/Spinner';
import { RootState } from '../../store';
import { loginUser } from './actions';

import './styles.css';

const mapState = (state: RootState) => ({
  error: state.loginPage.error,
  isLoading: state.system.isLoading
});

const mapDispatch = {
  loginUser
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;

const HomePage: React.FC<Props> = ({
  loginUser: loginUserProps,
  error,
  isLoading
}) => {
  const [user, setUser] = useState<{username: string, password: string}>({username: '', password: ''});

  const handleLogin = () => {
    loginUserProps(user.username, user.password);
  }

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    setUser({
      ...user,
      [name]: value
    })
  }

  if (isLoading)
    return (
      <Spinner />
    )

  return (
    <div className="login-wrapper">
      <div className="content">
        <label>
          <div className="label">
            Username
          </div>
          <input 
            placeholder="Enter your username"
            value={user.username}
            onChange={handleChange}
            name="username"
          />
        </label>
        <label>
          <div className="label">
            Password
          </div>
          <input 
            placeholder="Enter your password"
            value={user.password}
            onChange={handleChange}
            name="password"
          />
        </label>
        <div className="buttons">
          <Button
            type='success'
            onClick={handleLogin}
          >
            login
          </Button>
        </div>
        <ErrorElement 
          error={error}
        />
      </div>
    </div>
  );
}

export default connector(HomePage);
