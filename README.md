### Usage ###

docker-compose up --build

Server can be found on http://localhost:1337

## Images ##
* [backend](https://hub.docker.com/repository/docker/yaroslaww1/node-backend)
* [frontend](https://hub.docker.com/repository/docker/yaroslaww1/react-nginx-frontend)

## Users ##
- admin admin - admin
- user1 user1 - just user
